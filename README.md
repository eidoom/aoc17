# [aoc17](https://gitlab.com/eidoom/aoc17)

## Lua

I solved [the days for this year](https://adventofcode.com/2017) using [Lua](https://www.lua.org/).

Install the [LuaJIT](https://luajit.org/luajit.html) compiler on Fedora with
```shell
sudo dnf install luajit
```

We can use it in interpreter mode to
* test days like
    ```shell
    echo 91212129 | ./d01.lua
    ```
* run days like
    ```shell
    ./d01.lua < i01
    ```

Runtime may be less with precompiled bytecode
```shell
luajit -b d03a.lua d03a.raw
time echo 1e9 | ./d03a.lua
time echo 1e9 | luajit d03a.raw
```
