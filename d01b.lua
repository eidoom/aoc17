#!/usr/bin/env luajit

local input = io.stdin:read()
local sum = 0
for i = 1, #input, 1 do
	local j = i + #input / 2
	if j > #input then
		j = j - #input
	end
	local a = input:sub(i, i)
	local b = input:sub(j, j)
	if a == b then
		sum = sum + a
	end
end
print(sum)
