#!/usr/bin/env luajit

local sum = 0

while true do
	local input = io.stdin:read()
	if not input then
		break
	end

	local row = {}
	for a in input:gmatch("%d+") do
		table.insert(row, tonumber(a))
	end

	sum = sum + math.max(unpack(row)) - math.min(unpack(row))
end

print(sum)
