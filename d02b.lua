#!/usr/bin/env luajit

local sum = 0

while true do
	local input = io.stdin:read()
	if not input then
		break
	end

	local row = {}
	for a in input:gmatch("%d+") do
		table.insert(row, tonumber(a))
	end

	table.sort(row)

	for i, v in ipairs(row) do
		for j = i+1, #row, 1 do
			if row[j] % v == 0 then
				sum = sum + (row[j] / v)
				goto continue
			end
		end
	end
	:: continue ::
end

print(sum)
