#!/usr/bin/env luajit

local function spiral(fin)
	local x = 0
	local y = 0
	local z = 1
	local sign = 1
	local arm_length = 1
	local x_axis = true

	while true do
		for _ = 1, arm_length, 1 do
			z = z + 1
			x = x + sign
			if z == fin then
				return math.abs(x) + math.abs(y)
			end
		end

		if x_axis then
			x, y = y, x
		else
			sign = -sign
			arm_length = arm_length + 1
		end

		x_axis = not x_axis
	end
end

local input = tonumber(io.stdin:read())
print(spiral(input))
