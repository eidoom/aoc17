#!/usr/bin/env luajit

local function spiral(fin)
	local x = 0
	local y = 0
	local sign = 1
	local arm_length = 1
	local grid = {}
	grid[x..","..y] = 1

	while true do
		for _ = 1, arm_length, 1 do
			x = x + sign
			grid[x..","..y] = 0
			for dx = -1, 1, 1 do
				for dy = -1, 1, 1 do
					if not (dx == 0 and dy == 0) and grid[x+dx..","..y+dy] then
						grid[x..","..y] = grid[x..","..y] + grid[x+dx..","..y+dy]
					end
				end
			end

			if grid[x..","..y] > fin then
				return grid[x..","..y]
			end
		end

		for _ = 1, arm_length, 1 do
			y = y + sign
			grid[x..","..y] = 0
			for dx = -1, 1, 1 do
				for dy = -1, 1, 1 do
					if not (dx == 0 and dy == 0) and grid[x+dx..","..y+dy] then
						grid[x..","..y] = grid[x..","..y] + grid[x+dx..","..y+dy]
					end
				end
			end

			if grid[x..","..y] > fin then
				return grid[x..","..y]
			end
		end

		sign = -sign
		arm_length = arm_length + 1
	end
end

local input = tonumber(io.stdin:read())
print(spiral(input))
